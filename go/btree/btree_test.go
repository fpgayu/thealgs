package btree

import (
	"container/list"
	"fmt"
	"testing"
)

func buildBtree(arr []interface{}) *TreeNode {
	if len(arr) == 0 {
		return nil
	}
	root := &TreeNode{Val: arr[0].(int)}
	st := list.New()
	st.PushBack(root)

	for i := 1; i < len(arr); {
		elem := st.Front()
		st.Remove(elem)
		if elem.Value == nil {
			continue
		}

		node := elem.Value.(*TreeNode)

		if arr[i] != nil {
			node.Left = &TreeNode{Val: arr[i].(int)}
		}

		if node.Left != nil {
			st.PushBack(node.Left)
		} else {
			st.PushBack(nil)
		}
		i++

		if i < len(arr) && arr[i] != nil {
			node.Right = &TreeNode{Val: arr[i].(int)}
		}

		if node.Right != nil {
			st.PushBack(node.Right)
		} else {
			st.PushBack(nil)
		}
		i++
	}

	return root
}

func printBtree(root *TreeNode) {
	if root == nil {
		return
	}

	st := list.New()
	st.PushBack(root)
	cnt := 1

	fmt.Print("[ ")
	for cnt > 0 {
		elem := st.Front()
		st.Remove(elem)
		if elem.Value == nil {
			fmt.Print("null", " ")
			st.PushBack(nil)
			st.PushBack(nil)
			continue
		}

		node := elem.Value.(*TreeNode)
		fmt.Print(node.Val, " ")
		cnt--

		if node.Left != nil {
			st.PushBack(node.Left)
			cnt++
		} else {
			st.PushBack(nil)
		}

		if node.Right != nil {
			st.PushBack(node.Right)
			cnt++
		} else {
			st.PushBack(nil)
		}
	}
	fmt.Println("]")
}

func TestBtree(t *testing.T) {
	// case1 := buildBtree([]interface{}{3, 1, 4, nil, nil, 2})
	// recoverTree(case1)
	// printBtree(case1)

	// case2 := buildBtree([]interface{}{2, 1, nil, nil, 3})
	// recoverTree(case2)
	// printBtree(case2)

	// case3 := buildBtree([]interface{}{5, 4, 8, 11, nil, 13, 4, 7, 2, nil, nil, 5, 1})
	// t.Log(pathSum(case3, 22))

	case3 := buildBtree([]interface{}{3, 2, 4, 1})
	result := trimBST(case3, 1, 1)
	printBtree(result)
}
