package btree

// https://leetcode-cn.com/problems/validate-binary-search-tree/

func isValidBST(root *TreeNode) bool {
	// 中序遍历是一个升序的过程
	var prev *TreeNode

	var dfs func(*TreeNode) bool
	dfs = func(node *TreeNode) bool {
		if node == nil {
			return true
		}

		if !dfs(node.Left) {
			return false
		}

		if prev != nil && prev.Val >= node.Val {
			return false
		}
		prev = node

		if !dfs(node.Right) {
			return false
		}

		return true
	}

	return dfs(root)
}
