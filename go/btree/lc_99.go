package btree

// https://leetcode-cn.com/problems/recover-binary-search-tree/

// 递归
func recoverTree(root *TreeNode) {
	var prev *TreeNode
	var x, y *TreeNode

	var dfs func(*TreeNode)
	dfs = func(node *TreeNode) {
		if node == nil {
			return
		}

		dfs(node.Left)
		if prev == nil {
			prev = node
		} else {
			if node.Val < prev.Val {
				y = node
				if x == nil {
					x = prev
				}
			}
			prev = node
		}
		dfs(node.Right)
	}

	dfs(root)
	if x != nil && y != nil {
		x.Val, y.Val = y.Val, x.Val
	}
}
