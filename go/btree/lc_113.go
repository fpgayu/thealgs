package btree

func pathSum(root *TreeNode, targetSum int) [][]int {
	if root == nil {
		return nil
	}
	if root.Left == nil && root.Right == nil && root.Val == targetSum {
		return [][]int{{root.Val}}
	}
	left := pathSum(root.Left, targetSum-root.Val)
	right := pathSum(root.Right, targetSum-root.Val)
	for i := range left {
		left[i] = append(left[i], root.Val)
		copy(left[i][1:], left[i])
		left[i][0] = root.Val
	}
	for i := range right {
		right[i] = append(right[i], root.Val)
		copy(right[i][1:], right[i])
		right[i][0] = root.Val
	}
	if len(left) > 0 {
		left = append(left, right...)
	} else {
		left = right
	}
	return left
}

// func pathSum(root *TreeNode, targetSum int) [][]int {
// 	result := make([][]int, 0)
// 	dfs(root, targetSum, make([]int, 0), &result)
// 	return result
// }

// func dfs(root *TreeNode, targetSum int, path []int, result *[][]int) {
// 	if root == nil {
// 		return
// 	}
// 	tmp := make([]int, len(path))
// 	copy(tmp, path)
// 	path = tmp
// 	path = append(path, root.Val)
// 	targetSum -= root.Val
// 	if root.Left == nil && root.Right == nil && targetSum == 0 {
// 		*result = append(*result, path)
// 		return
// 	}
// 	dfs(root.Left, targetSum, path, result)
// 	dfs(root.Right, targetSum, path, result)
// }
