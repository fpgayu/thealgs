package sort

func BubbleSort(arr []int) []int {
	for j := len(arr) - 1; j >= 0; j-- {
		for i := 0; i < j; i++ {
			if arr[i] > arr[i+1] {
				arr[i], arr[i+1] = arr[i+1], arr[i]
			}
		}
	}

	return arr
}

