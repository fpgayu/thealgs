package sort

func QuickSort(arr []int) []int {
	QuickSortRange(arr, 0, len(arr)-1)
	return arr
}

func QuickSortRange(arr []int, low, high int) {
	if len(arr) <= 1 {
		return
	}

	if low < high {
		pivot := partition(arr, low, high)
		QuickSortRange(arr, low, pivot-1)
		QuickSortRange(arr, pivot, high)
	}
}

func partition(arr []int, low, high int) int {
	index := low
	pivotElem := arr[high]
	for i := low; i < high; i++ {
		if arr[i] <= pivotElem {
			arr[index], arr[i] = arr[i], arr[index]
			index++
		}
	}
	arr[index], arr[high] = arr[high], arr[index]
	return index
}
